window.App = {}

eventify(window.App)

function eventify(target) {
    if (typeof target != 'object') return false;
    target = target || {};
    target.callbacks = {};
    target.on = (eventName, callback) => {
        if (!eventName || !callback) return target;

        let base, events;

        if (!target.hasOwnProperty('callbacks')) target.callbacks || (target.callbacks = {});

        events = eventName.split(' ');
        events.map(eventName => {
            (base = target.callbacks)[eventName] || (base[eventName] = []);
            target.callbacks[eventName].push(callback);
        });

        return target;
    };

    target.emit = (...args) => {
        let list, events, eventName;

        eventName = args.shift();
        events = eventName.split(' ');

        events.map(eventName => {
            list = target.callbacks !== null ? target.callbacks[eventName] || [] : [];

            if (list.length) list.map(item => item && item.apply(target, args));
        });
    };
}

App.on('app.ready', (data) => {
    $('body').append(`<div>${data}</div>`)
})
