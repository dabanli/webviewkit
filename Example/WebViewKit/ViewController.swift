//
//  ViewController.swift
//  WebViewKit
//
//  Created by 大板栗 on 10/31/2019.
//  Copyright (c) 2019 大板栗. All rights reserved.
//

import UIKit
import WebViewKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(open))
        view.addGestureRecognizer(gesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK - extension

extension ViewController {
    @objc
    func open() {
        let vc = WebViewController()
        vc.isOnline = true
        vc.url = "isOnline"
        navigationController?.pushViewController(vc, animated: true)
    }
}
