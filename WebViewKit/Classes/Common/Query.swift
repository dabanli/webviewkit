//
//  Query.swift
//  JustWebView
//
//  Created by 大板栗 on 2018/9/8.
//  Copyright © 2018 大板栗. All rights reserved.
//

import Foundation

public class Query {
    
    public static func parse(_ query: String) -> [String: String] {
        guard query.contains("=") else {
            return [:]
        }
        
        var decoded = [(key: String, value: String)]()
        var queryClone = query
        
        if queryClone.hasPrefix("?") {
            queryClone.remove(at: queryClone.startIndex)
        }
        
        let nextPairStartRange = queryClone.range(of: "&")
        let firstPairEndIndex = nextPairStartRange?.lowerBound ?? queryClone.endIndex
        
        guard let firstKeyValuePair = extractPairs(string: queryClone, endIndex: firstPairEndIndex) else {
            return [:]
        }
        
        decoded.append(firstKeyValuePair)
        
        while let pairStartRange = queryClone.range(of: "&") {
            
            let stripped = queryClone.substring(from: pairStartRange.upperBound)
            
            let nextPairStartRange = stripped.range(of: "&")
            let pairEndIndex = nextPairStartRange?.lowerBound ?? stripped.endIndex
            
            guard let keyValuePair = extractPairs(string: stripped, endIndex: pairEndIndex ) else {
                break
            }
            
            decoded.append(keyValuePair)
            queryClone = stripped.substring(from: pairEndIndex)
            
        }
        
        return [String: String](decoded)
    }
    
    static func extractPairs(string: String, endIndex: String.Index) -> (key: String, value: String)? {
        guard let equalRange = string.range(of: "=") else {
            return nil
        }
        
        let keyRange = string.startIndex ..< equalRange.lowerBound
        let valueRange = equalRange.upperBound ..< endIndex
        
        let key = string.substring(with: keyRange)
        let value = string.substring(with: valueRange)
        
        return (key,value)
    }
}


extension Dictionary {
    
    fileprivate init(_ sequence: [(key: Key, value: Value)]) {
        self = sequence.reduce([Key:Value]()) { $0 + [$1.key : $1.value] }
    }
    
    fileprivate static func + (lhs: [Key:Value], rhs: [Key:Value]) -> [Key:Value] {
        var base = lhs
        rhs.forEach { base[$0.key] = $0.value }
        return base
    }
}

