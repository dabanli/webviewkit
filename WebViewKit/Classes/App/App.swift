//
//  App.swift
//  JustWebView
//
//  Created by 大板栗 on 2018/9/7.
//  Copyright © 2018 大板栗. All rights reserved.
//

import UIKit

public final class App {
    public class func setStatusBarColor(_ color: UIColor) {
        let statusBar = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = color
    }
    public class func dictionaryToJSON(_ dictionary: Dictionary<String, Any>, _ onSuccess: (_ string: String) -> ()) {
        if let data = try? JSONSerialization.data(withJSONObject: dictionary, options: []) {
            if let string = String(data: data, encoding: .ascii) {
                onSuccess(string)
            } else {
                print("no JSON string")
            }
        } else {
            print("no JSON data")
        }
    }
}

public func isX() -> Bool {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                return false
            case 2436, 2688, 1792:
                return true
            default:
                return false
        }
    } else {
        return false
    }
}
