//
//  WKWebView.swift
//  JustWebView
//
//  Created by 大板栗 on 2018/9/8.
//  Copyright © 2018 大板栗. All rights reserved.
//

import Foundation
import WebKit

extension WKWebView {
    func open(_ string: String, isOnline: Bool = false) {
        isOnline ? loadUrl(string) :loadResource(string)
    }
    
    func loadUrl(_ string: String) {
        if let url = URL(string: string) {
            load(URLRequest(url: url))
        }
    }
    
    func loadResource(_ string: String) {
        let url = Bundle.main.url(forResource: string, withExtension: "html")
        if let url = url {
            let request: NSURLRequest = NSURLRequest(url: url)
            load(request as URLRequest)
        } else {
            print("html file not found")
        }
    }
}
