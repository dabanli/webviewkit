//
//  Command.swift
//  JustWebView
//
//  Created by 大板栗 on 2018/9/8.
//  Copyright © 2018 大板栗. All rights reserved.
//

import Foundation
import WebKit

extension WebViewController {
    func register(_ method: String = "", query: String = "") {
        let parsed = Query.parse(query)
        
        switch method {
        case "to":
            to(parsed)
        case "set":
            set(parsed)
        default:
            print("Method \(method) not registered.")
        }
    }
}

extension WebViewController {
    func to(_ parsed: [String: String]) {
        print("to")
        
        let next = WebViewController()
        
        if let isOnline = parsed["isOnline"], isOnline == "true" || isOnline == "false" {
            next.isOnline = isOnline == "true" ? true : false
        } else {
            next.isOnline = false
        }
        
        if let url = parsed["url"] {
            next.url = url
        } else {
            print("url in nil")
        }
        
        navigationController?.pushViewController(next, animated: true)
    }
    
    func set(_ parsed: [String: String]) {
        if let bounces = parsed["bounces"], bounces == "true" || bounces == "false" {
            webview.scrollView.bounces = bounces == "true" ? true : false;
        }
    }
}
